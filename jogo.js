var vitoria = false;
var rodadas = 0;
var jogador = 0;
const infoJogadores = [{
  nome: 'Meninas super poderosas'
}, {
  nome: 'Macaco louco'
}];

function verificarGanhador(casa) {
  const possibilidades = {
    '1': [[1, 2, 3], [1, 5, 9], [1, 4, 7]],
    '2': [[1, 2, 3], [2, 5, 8]],
    '3': [[1, 2, 3], [7, 5, 3], [3, 6, 9]],
    '4': [[4, 5, 6], [1, 4, 7]],
    '5': [[4, 5, 6], [1, 5, 9], [3, 5, 7], [2, 5, 8]],
    '6': [[4, 5, 6], [3, 6, 9]],
    '7': [[7, 8, 9], [3, 5, 7], [1, 4, 7]],
    '8': [[7, 8, 9], [2, 5, 8]],
    '9': [[7, 8, 9], [1, 5, 9], [3, 6, 9]]
  };

  let possibilidadeAtual = possibilidades[casa];

  for(let n = 0; n < possibilidadeAtual.length; n++) {
    let [a, b, c] = possibilidadeAtual[n];
    let casa1 = document.getElementById(`casa${a}`).style.backgroundImage;
    let casa2 = document.getElementById(`casa${b}`).style.backgroundImage;
    let casa3 = document.getElementById(`casa${c}`).style.backgroundImage;

    if(casa1 == casa2 && casa2 == casa3 && casa1 != '') {
      return jogador;
    };
  };

  return null;
};

function novaPartida() {
	rodadas = 0;
	vitoria = false;

	if(jogador === 0) {
		jogador = 1;
	} else {
		jogador = 0;
	}


	document.getElementById('status').innerHTML = `Vez: <span id="jogador" class="${infoJogadores[jogador].nome}">${infoJogadores[jogador].nome}</span>`;
	document.getElementById('state').style.display = 'none';
	const divisoes = document.getElementsByClassName('casa');

	for( let pos = 0; pos < divisoes.length; pos++ ) {
    	divisoes[pos].style.backgroundImage = '';
  };
  document.getElementById('jogar-novamente').style.display = 'none';
};

function clique(id) {
  let casa = document.getElementById(`casa${id}`);

  if((casa.style.backgroundImage == '' || casa.style.backgroundImage == null) && vitoria == false) {
    rodadas += 1;
    casa.style.backgroundImage = `url('./img/${jogador}.jpg')`;

    if(rodadas >= 5) {
      var vencedor = verificarGanhador(id);
      if(vencedor != null) {
        vitoria = true;
        document.getElementById('status').innerHTML = `Ganhador: <span id="jogador" class="${vencedor}">${infoJogadores[vencedor].nome}</span`;
        document.getElementById('jogar-novamente').style.display = 'inline';

        if(infoJogadores[vencedor].nome == 'Macaco louco'){
  				document.getElementById('state').style.backgroundImage = "url('./img/3.png')";
  			} else if (infoJogadores[vencedor].nome == 'Meninas super poderosas') {
  				document.getElementById('state').style.backgroundImage = "url('./img/2.png')";
  			};
        document.getElementById('state').style.display = 'flex';


        return;
      };

      if(rodadas == 9 && vencedor == null) {
        vitoria = true;
        document.getElementById('status').innerHTML = `Empate!`;
        document.getElementById('state').style.display = 'flex';
        document.getElementById('jogar-novamente').style.display = 'inline';
        document.getElementById('state').style.backgroundImage = "url('./img/4.png')";

        return;
      };
    };

    if(jogador) {
      jogador = 0;
      document.getElementById('status').innerHTML = `Vez: <span id="jogador" class="${infoJogadores[jogador].nome}">${infoJogadores[jogador].nome}</span>`;
    } else {
      jogador = 1;
      document.getElementById('status').innerHTML = `Vez: <span id="jogador" class="${infoJogadores[jogador].nome}">${infoJogadores[jogador].nome}</span>`;
    };
  };
};
